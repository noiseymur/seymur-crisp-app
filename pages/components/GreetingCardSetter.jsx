import React, {useState, useRef, useCallback} from "react";
import {
  Card,
  Button,
  Banner,
  Spinner,
  Stack,
  Heading,
  Form,
  TextField,
  DatePicker,
  ColorPicker,
  ButtonGroup,
  Toast,
  VisuallyHidden
} from "@shopify/polaris";
import { useMutation } from "react-apollo";
import gql from "graphql-tag";

const GreetingCardSetter = ({productIds, setSelectedIds}) => {

  const ADD_GREETING_CARD = gql`
    mutation metafieldsSet($metafields: [MetafieldsSetInput!]!) {
      metafieldsSet(metafields: $metafields) {
        metafields {
          namespace
        }
      }
    }
  `;

  const [addGreeting, {loading, data}] = useMutation(ADD_GREETING_CARD,{
    onCompleted: ()=> setToastContent("Added the card successfully"),
    onError: ()=> setToastContent("Error occurred")
  });
  const [toastContent, setToastContent] = useState("");
  const submitBtn = useRef(null);

  const [text,setText] = useState("");
  const [date,setDate] = useState("");
  const [color,setColor] = useState({
    hue: 120,
    brightness: 1,
    saturation: 1,
  });

  const newDate = useRef(new Date());
  const [{month, year}, setMonthYear] = useState(
    {month: newDate.current.getUTCMonth(), year: newDate.current.getUTCFullYear()}
  );

  const handleTextChange = useCallback((newValue)=>setText(newValue));
  const handleDateChange = useCallback((newValue)=>setDate(newValue));
  const handleColorChange = useCallback((newValue)=>setColor(newValue));
  const handleMonthChange = useCallback((month, year)=>setMonthYear({month, year}),[]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = [];
    if(!text) errors.push("Greeting text");
    if(!date) errors.push("Date");
    if(errors.length) {
      const errorText = errors.reduce((acc,item,idx)=> idx>0 ? acc + ` and ${item}` : acc + item, "") + ` ${errors.length>1 ? 'are' : 'is'} empty`;
      setToastContent(errorText);
      return;
    } else if(toastContent) {
      setToastContent("");
    }

    const hexColor = hslToHex(color.hue, color.saturation, color.brightness);

    productIds.forEach(id=>{
      const metaFields = [
        ["text","multi_line_text_field",text],
        ["date","date_time",date.start],
        ["color","color",hexColor]
      ].map(item=>({
        key: item[0],
        namespace: "greeting_card",
        ownerId: id,
        type: item[1],
        value: item[2]
      }));
      addGreeting({ variables: { metafields: metaFields }});
    });
  }

  const hslToHex = useCallback((h,s,l)=>{
    l /= 100;
    const a = s * Math.min(l, 1 - l) / 100;
    const f = n => {
      const k = (n + h / 30) % 12;
      const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
    };
    return `#${f(0)}${f(8)}${f(4)}`;
  });

  return(
    <Card>
      {toastContent ? <Toast content={toastContent} onDismiss={()=>setToastContent("")} /> : ""}
      <Card.Section>
        <Stack distribution="equalSpacing" alignment="center">
          <Heading element="h2">Add a greeting card to selected {productIds?.length > 1 ? "products" : "product"}</Heading>
          <ButtonGroup>
            <Button disabled={loading} onClick={()=> setSelectedIds([]) } >{data ? "Back to products" : "Cancel"}</Button>
            <Button disabled={loading} primary onClick={()=> submitBtn.current.click() }>Save</Button>
          </ButtonGroup>
        </Stack>
      </Card.Section>
      {loading ? 
      <Card.Section>
        <Banner status="info">
          <Stack>
            <Spinner size="small" /> <span>Loading</span>
          </Stack>
        </Banner>
      </Card.Section>
      : ""}
      <Card.Section>
        <Form
          onSubmit={handleSubmit}
        >
          <div className="fields-container">
            <div className="field text-field">
              <div className="label">Greeting text</div>
              <TextField
                type="text"
                value={text}
                onChange={handleTextChange}
                autoComplete="off"
                multiline={6}
              />
            </div>
            <div className="field date-field">
              <div className="label">Birthday date</div>
              <div className="date-component">
                <DatePicker
                  month={month}
                  year={year}
                  onChange={handleDateChange}
                  onMonthChange={handleMonthChange}
                  selected={date}
                  disableDatesBefore={new Date(Date.now() - 86400000)}
                />
              </div>
            </div>
            <div className="field color-field">
              <div className="label">Color</div>
              <ColorPicker
                color={color}
                onChange={handleColorChange}
                fullWidth
              />
            </div>
          </div>
          <VisuallyHidden>
            <button ref={submitBtn} type="submit"></button>
          </VisuallyHidden>
        </Form>
      </Card.Section>
    </Card>
  )
}

export default GreetingCardSetter;