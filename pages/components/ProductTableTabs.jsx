import React, { useState, useCallback } from "react";
import { Tabs } from "@shopify/polaris";

const ProductTableTabs = ({onUpdate}) => {
  const [selected,setSelected] = useState(0);

  const handleTabChange = useCallback(
    (tabIndex)=>{
      setSelected(tabIndex);
      onUpdate(tabIndex ? tabs[tabIndex].content : "");
    },[]
  );

  const tabs = [
    {
      id: 'all-products-1',
      content: 'All',
      accessibilityLabel: 'All products',
      panelID: 'all-products-content-1',
    },
    {
      id: 'active-products-1',
      content: 'Active',
      panelID: 'active-products-content-1',
    },
    {
      id: 'draft-products-1',
      content: 'Draft',
      panelID: 'draft-products-content-1',
    },
    {
      id: 'archived-products-1',
      content: 'Archived',
      panelID: 'archived-products-content-1',
    }
  ];

  return <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} />


}

export default ProductTableTabs;