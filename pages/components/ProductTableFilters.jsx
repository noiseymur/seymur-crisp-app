import React , { useState, useEffect, useRef } from "react";
import { Filters, ChoiceList, TextField } from "@shopify/polaris";
import { useQuery } from "react-apollo";
import gql from "graphql-tag";

const ProductTableFilters = (props) => {

  const PRODUCT_VENDORS = gql`
    query productVendors {
      shop {
        productVendors(first: 200) {
          edges {
            node
          }
        }
      }
    }
  `;

  const { data: vendorsData } = useQuery(PRODUCT_VENDORS);
  
  const [vendorsList,setVendorsList] = useState([]);
  const [appliedFilters, setAppliedFilters] = useState([]);
  const updateQuery = useRef(null);

  const [queryValue, setQueryValue] = useState("");
  const [vendor, setVendor] = useState(null);
  const [taggedWith, setTaggedWith] = useState("");
  
  useEffect(()=>{
    const data = vendorsData?.shop?.productVendors?.edges
    .map(item=>({label: item.node, value: item.node}));
    setVendorsList(data || []);
  },[vendorsData]);

  const filters = [
    {
      key: 'productVendor',
      label: 'Product vendor',
      filter: (
          <ChoiceList
            title="Product Vendor"
            titleHidden
            choices={vendorsList}
            selected={vendor ? [`${vendor}`] : []}
            onChange={(value)=>setVendor(value)}
          />
      ),
      shortcut: true,
    },
    {
      key: 'tag',
      label: 'Tagged with',
      filter: (
        <TextField
          title="Tagged with"
          titleHidden
          value={taggedWith}
          clearButton={true}
          onChange={(val)=>setTaggedWith(val)}
          onClearButtonClick={()=>setTaggedWith("")}
        />
        ),
        shortcut: true
    }
  ];

  useEffect(()=>{
    if(updateQuery.current){
      clearTimeout(updateQuery.current);
    }
    updateQuery.current = setTimeout(() => {
      updateQuery.current = null;
      const filters = [];
      if(vendor) {
        filters.push({
          key: 'vendor',
          value: vendor,
          label: `Vendor: ${vendor}`,
          onRemove: ()=>{
            setVendor(null);
          }
        })
      }
      if(taggedWith) {
        filters.push({
          key: "tag",
          value: taggedWith,
          label: `Tagged with: ${taggedWith}`,
          onRemove: ()=>{
            setTaggedWith("");
          }
        })
      }
      if(queryValue) {
        filters.push({
          key: "title",
          value: `*${queryValue}*`,
          label: `Title containing: ${queryValue}`,
          onRemove: ()=>{
            setQueryValue("");
          }
        })
      }
      setAppliedFilters(filters);
      props.onUpdate(filters);
    }, 300);
  },[taggedWith,vendor, queryValue]);

return (
  <Filters queryValue={queryValue} onQueryChange={(value)=>setQueryValue(value)} appliedFilters={appliedFilters} filters={filters}/>
)

}

export default ProductTableFilters;