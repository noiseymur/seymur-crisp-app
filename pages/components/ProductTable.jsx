import React from "react";
import {
  IndexTable,
  useIndexResourceState,
  Thumbnail,
  TextStyle
} from "@shopify/polaris";

const ProductTable = ({products, loading, setSelectedIds}) => {

  const rowPadding = {
    padding: '10px 0'
  }

  const resourceName = {
    singular: "product",
    plural: "products"
  };

  const {selectedResources, allResourcesSelected, handleSelectionChange} =
    useIndexResourceState(products);

  const promotedBulkActions = [
    {
      content: "Add a greeting card",
      onAction: ()=> setSelectedIds(selectedResources)
    }
  ]

  const rowMarkup = products.map(({id, featuredImage, title, status, totalInventory, productType, vendor}, index)=>(
    <IndexTable.Row
      id={id}
      key={id}
      position={index}
      selected={selectedResources.includes(id)}
    >
      <IndexTable.Cell>
        <div style={rowPadding}>
          <Thumbnail size="small" source={featuredImage.transformedSrc} alt=""/>
        </div>
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={rowPadding}>
          <h3>
            <TextStyle variation="strong">{title}</TextStyle>
          </h3>
        </div>
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={rowPadding}>
          {status}
        </div>
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={rowPadding}>
          {totalInventory}
        </div>
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={rowPadding}>
          {productType}
        </div>
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={rowPadding}>
          {vendor}
        </div>
      </IndexTable.Cell>
    </IndexTable.Row>
  ))

  return (
    <IndexTable
      resourceName={resourceName}
      itemCount={products.length}
      selectedItemsCount={
        allResourcesSelected ? 'All' : selectedResources.length
      }
      onSelectionChange={handleSelectionChange}
      promotedBulkActions={promotedBulkActions}
      loading={loading}
      headings={[
        {title: ""},
        {title: "Product"},
        {title: "Status"},
        {title: "Inventory"},
        {title: "Type"},
        {title: "Vendor"}
      ]}
    >
      {rowMarkup}
    </IndexTable>
  )
}

export default ProductTable;