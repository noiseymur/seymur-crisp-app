import React, { useState, useEffect } from "react";
import ProductTableTabs from "./components/ProductTableTabs";
import ProductTableFilters from "./components/ProductTableFilters";
import ProductTable from "./components/ProductTable";
import GreetingCardSetter from "./components/GreetingCardSetter";
import { Page, Card, Frame } from "@shopify/polaris";
import gql from "graphql-tag";
import { useQuery } from "react-apollo";

const Index = () => {

  const [products, setProducts] = useState([]);
  const [tabFilter, setTabFilter] = useState(null);
  const [filters, setFilters] = useState([]);
  const [selectedIds, setSelectedIds] = useState([]);

  const GET_PRODUCTS = gql`
    query getProducts($query: String){
      products(first: 100, query: $query) {
        edges {
          node {
            id
            title
            status
            totalInventory
            productType
            vendor
            featuredImage {
              id
              transformedSrc
            }
          }
        }
      }
    }
  `;

  const {loading, error, data = {}, refetch} = useQuery(GET_PRODUCTS);

  useEffect(()=>{
    const query = '';
    if(tabFilter){
      query = `${query} status:${tabFilter}`;
    }
    if(filters.length){
      filters.forEach(filter=>{
        query = `${query} ${filter.key}:${filter.key==="title" ? filter.value : `"${filter.value}"`}`
      });
    }
    refetch({query});
  },[tabFilter,filters]);

  useEffect(()=>{
    const newData = data?.products?.edges?.map(edge=>edge.node);
    if(Array.isArray(newData)) setProducts(newData);
  },[data]);

  return (error ? "Error" :
  <Frame>
    {selectedIds.length ?
      <Page>
        <GreetingCardSetter productIds={selectedIds} setSelectedIds={setSelectedIds}/>
      </Page>
      :
      <Page fullWidth>
        <Card>
          <ProductTableTabs onUpdate={setTabFilter}/>
          <Card.Section>
            <ProductTableFilters onUpdate={setFilters}/>
            <ProductTable loading={loading} products={products} setSelectedIds={setSelectedIds}/>
          </Card.Section>
        </Card>
      </Page>
    }
  </Frame>
  );
};

export default Index;
