import mongoose from "mongoose";

const SessionSchema = mongoose.Schema({
  id: String,
  payload: JSON
}, {strict: false});

export default mongoose.model('Sessions',SessionSchema);